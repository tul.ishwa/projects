A simple music lights ball that is placed on a sub-woofer. It synch's its lights with the sound from the sub-woofer during music playback 

Procedure
1. Extract the zip file.
2. Build the circuit as shown in circuit_music_lights.png
3. Download and install latest version of CASP from here: https://aadhuniklabs.com/?page_id=550 
4. Run CASP.
5. From CASP, open the project from the extracted zip file and open workspace file wsp0.wsp.
6. Open Setup Simulation Parameters and set the hardware programmer port to the serial port where Arduino Nano is connected.
7. Build the model and program the board.
8. After the board is programmed place the assembly into a plastic ball of suitable size. Ensure that the accelerometer is placed firmly touching the bottom surface of the ball to get maximum sensitivity to the vibrations produced by the sub-woofer.
9. Place the ball on top of a sub-woofer and enjoy the music with lights.
10. Check this link on how to install CASP: https://aadhuniklabs.com/?page_id=554
11. Check out this video: https://youtube.com/shorts/WyXcbJzsY8I
