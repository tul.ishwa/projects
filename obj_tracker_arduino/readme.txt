Procedure
1. Extract the two zip files.
2. Build the circuit as shown in obj_tracker_circuit.png
3. Adjust the camera to match with center angle of both the servos.
4. Connect Arduino Uno and USB camera to the PC USB ports.
5. Open CASP.
6. From CASP, open the project from arduino_model directory and open workspace file wsp0.wsp.
7. Open Setup Simulation Parameters and set the hardware programmer port to the serial port where Arduino Uno is connected.
8. Build the model and program Arduino Uno.
9. After the board is programmed the on-board led should blink at around 2Hz frequency.

10. Close the project and open project from native_model directory.
11. Open workspace file wsp0.wsp.
12. Open 'Simulation->Configure Simulation IO' window and replace COM14 (by double clicking on it) 
    with the serial port where Arduino Uno is connected.
13. Build and run the model. A simulation panel window opens up and shows the camera view.
14. Take an object towards the camera and try to move it. Camera should move along with the object.
15. Try to adjust the PID controller block parameters if slow response is desired.
16. If any offset errors are encountered try to set suitable offsets to offset_x and offset_y blocks.

check out this youtube link: https://youtu.be/wKvix27EGBI
