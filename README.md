**Repository Name: projects**
**Location: https://gitlab.com/tul.ishwa/projects**
**Username: tul.ishwa**

**Some of the below projects uses CASP. Install CASP from https://aadhuniklabs.com for building and executing these projects**

# This repository consists of following projects

1. Object tracker with Arduino Uno and USB camera
2. Vibration Based Sub-Woofer Music Lights
3. Safeguarding home deliverey items from stray animals
